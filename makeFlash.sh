#!/bin/sh
#make .flash out of .sof for stratix V development kit

#Check input file
if [ -z "$1" ]; then 
  echo "filename:"; 
  read filename; 
else 
  filename=$1; 
fi

extension="${filename##*.}"
filename="${filename%.*}"

if [ "$extension" != "sof" ]; then 
  echo "The extension .$extension is not supported. Please use .sof files";
  exit 0;
fi

#check output file
if [ -z "$2" ]; then
  out_filename=$filename.flash;
else
  out_filename=$2;
fi

out_extension="${out_filename##*.}"
out_filename="${out_filename%.*}"
if [ "$out_extension" != "flash" ]; then 
  echo "The output file has to have a .flash ending. Changed $out_file to $out_filname.flash";
fi

#check if file exists
if [ -f $filename.sof ]; then
  echo "Convert $filename.sof to $out_filename.flash:";
  sof2flash --input=$filename.sof --output=$out_filename.flash --offset=0x020A0000 --pfl --optionbit=0x00030000 --programmingmode=PS
else
  echo "$filename.sof not found";
  exit 0;
fi

if [ -f $out_filename.flash ]; then
  echo "$out_filename.flash created";
else
  echo "Couldnt create $out_filename.flash";
fi


