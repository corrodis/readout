library IEEE;
use IEEE.std_logic_1164.all;


-- entity
entity clk_2 is
	Port (
	clk 		: in	 std_logic;
	div_en	: in   std_logic;
	s_clk		: out  std_logic);
end clk_2;

-- architecture
architecture ar2_clk_div of clk_2 is
	type count is range 0 to 2;
	constant max_count : count := 1;
	signal tmp_s_clk : std_logic;
	begin
		div: process (clk,div_en)
		variable counter : count := 0;
		begin
			if (div_en = '0') then
				 counter := 0;
				 tmp_s_clk <= '1';
			elsif (rising_edge(clk)) then
				-- div is enabled
				if (counter = max_count) then
					tmp_s_clk <= not tmp_s_clk;
					counter := 0;
				else
					counter := counter + 1;
				end if;
		end if;
	end process div;
	s_clk <= tmp_s_clk;
end ar2_clk_div;
					