-- Simon Corrodi
-- 8bit D flipflop

library IEEE;
use IEEE.std_logic_1164.all;

entity ff8bit is
	port ( D		: in	std_logic_vector(7 downto 0);
			 S 	: in  std_logic;
			 CLK 	: in  std_logic;
			 Q		: out std_logic_vector(7 downto 0));
end ff8bit;

architecture my_ff8bit of ff8bit is
begin
	ff: process (CLK)
	begin
		if(rising_edge(CLK)) then
			if( S = '0' ) then
				Q <= "00000000";
			else
				Q <= D;
			end if;
		end if;
	end process ff;
end my_ff8bit;


	