-- Simon Corrodi
-- based on free range vhdl book
-- divides the clock freq

-- library declaration
library IEEE;
use IEEE.std_logic_1164.all;

-- entity
entity clk_50Hz is
	Port (
	clk 		: in	 std_logic;
	div_en	: in   std_logic;
	s_clk		: out  std_logic);
end clk_50Hz;

-- architecture
architecture ar_clk_div of clk_50Hz is
	type count is range 0 to 1000001;
	constant max_count : count := 1000000;
	signal tmp_s_clk : std_logic;
	begin
		div: process (clk,div_en)
		variable counter : count := 0;
		begin
			if (div_en = '0') then
				 counter := 0;
				 tmp_s_clk <= '1';
			elsif (rising_edge(clk)) then
				-- div is enabled
				if (counter = max_count) then
					tmp_s_clk <= not tmp_s_clk;
					counter := 0;
				else
					counter := counter + 1;
				end if;
		end if;
	end process div;
	s_clk <= tmp_s_clk;
end ar_clk_div;
				 
