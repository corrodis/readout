-- Generates 32bit pseudo random numbers
-- taps: 32, 31, 29, 1
-- seed: 0xD0000001u
-- Simon Corrodi, corrodi@physi.uni-heidelberg.de, 23/10/2013
library IEEE;
use IEEE.std_logic_1164.all;

entity rnGen is 
	Port ( CLK 	: in	std_logic;
			 RST	: in  std_logic;
			 EN	: in  std_logic;
			 rn_out	: out std_logic_vector(31 downto 0));
end rnGen;

architecture ar_rnGen of rnGen is

	constant seed	: std_logic_vector(31 downto 0) := "01000001111100001111000111001101";							-- debug
	-- constant seed	: std_logic_vector(31 downto 0) := x"D0000001";
	signal   rn 	: std_logic_vector(31 downto 0);

begin

	sync: process(CLK) begin
		if (RST = '1') then
			-- synchronous clear, reset rn to seed
			rn <= seed;
		elsif (rising_edge(CLK)) then
			if (EN = '1') then
				-- if EN generate new rn every rising edge of CLK
				rn <= rn(30 downto 0) & ((rn(31) xor rn(30)) xor (rn(29) xor rn(0)));
			else
				-- if not enabled, keep old rn, do nothing
				rn <= rn;
			end if;
		end if;
	end process sync;
	
	rn_out <= rn;

end ar_rnGen;			 