-- Wrapper for transiver, includes Megafunctions: transiver, resert and reconfig
-- Simon Corrodi, corrodi@physi.uni-heidelberg.de, 23/10/2013

library IEEE;
use IEEE.std_logic_1164.all;

entity tx_wrapper is
	Port ( CLK			: in 	std_logic;
			 RST			: in  std_logic;
			 data			: in	std_logic_vector(31 downto 0);
			 tx_signal 	: out std_logic;
			 clk_out		: out std_logic;
			 ready		: out std_logic;
			 busy			: out std_logic
	);
end tx_wrapper;

architecture ar_tx of tx_wrapper is

	component transiver24 
		port (
			pll_powerdown        : in  std_logic_vector(0 downto 0);								-- OPT   pll_powerdown.pll_powerdown
			tx_analogreset       : in  std_logic_vector(0 downto 0); 							-- OPT   tx_analogreset.tx_analogreset
			tx_digitalreset      : in  std_logic_vector(0 downto 0); 							-- OPT   tx_digitalreset.tx_digitalreset
			tx_pll_refclk        : in  std_logic_vector(0 downto 0); 							-- OPT   tx_pll_refclk.tx_pll_refclk
			tx_pma_clkout        : out std_logic_vector(0 downto 0);                      --       tx_pma_clkout.tx_pma_clkout
			tx_serial_data       : out std_logic_vector(0 downto 0);                      --       tx_serial_data.tx_serial_data
			tx_pma_parallel_data : in  std_logic_vector(79 downto 0); 							-- OPT	tx_pma_parallel_data.tx_pma_parallel_data
			pll_locked           : out std_logic_vector(0 downto 0);                      --       pll_locked.pll_locked
			tx_cal_busy          : out std_logic_vector(0 downto 0);                      --       tx_cal_busy.tx_cal_busy
			reconfig_to_xcvr     : in  std_logic_vector(139 downto 0); 							-- OPT   reconfig_to_xcvr.reconfig_to_xcvr
			reconfig_from_xcvr   : out std_logic_vector(91 downto 0)                      --   		reconfig_from_xcvr.reconfig_from_xcvr
		);
	end component transiver24;

	component tx_reconf 
		port (
			reconfig_busy             : out std_logic;                                    --      reconfig_busy.reconfig_busy
			mgmt_clk_clk              : in  std_logic;             								-- OPT  mgmt_clk_clk.clk
			mgmt_rst_reset            : in  std_logic;             								-- OPT  mgmt_rst_reset.reset
			reconfig_mgmt_address     : in  std_logic_vector(6 downto 0); 						-- OPT  reconfig_mgmt.address
			reconfig_mgmt_read        : in  std_logic;             								-- OPT               .read
			reconfig_mgmt_readdata    : out std_logic_vector(31 downto 0);                --                   .readdata
			reconfig_mgmt_waitrequest : out std_logic;                                    --                   .waitrequest
			reconfig_mgmt_write       : in  std_logic;             								-- OPT               .write
			reconfig_mgmt_writedata   : in  std_logic_vector(31 downto 0); 					-- OPT               .writedata
			reconfig_to_xcvr          : out std_logic_vector(139 downto 0);               --   	 reconfig_to_xcvr.reconfig_to_xcvr
			reconfig_from_xcvr        : in  std_logic_vector(91 downto 0)  					-- 	 reconfig_from_xcvr.reconfig_from_xcvr
		);
	end component tx_reconf;

	component tx_reset is
		port (
			clock           : in  std_logic;             											-- OPT  clock.clk
			reset           : in  std_logic;             											-- OPT  reset.reset
			pll_powerdown   : out std_logic_vector(0 downto 0);                    			--   	  pll_powerdown.pll_powerdown
			tx_analogreset  : out std_logic_vector(0 downto 0);                    			--  	  tx_analogreset.tx_analogreset
			tx_digitalreset : out std_logic_vector(0 downto 0);                    			-- 	  tx_digitalreset.tx_digitalreset
			tx_ready        : out std_logic_vector(0 downto 0);                    			--      tx_ready.tx_ready
			pll_locked      : in  std_logic_vector(0 downto 0); 									-- OPT  pll_locked.pll_locked
			pll_select      : in  std_logic_vector(0 downto 0); 									-- OPT  pll_select.pll_select
			tx_cal_busy     : in  std_logic_vector(0 downto 0)  									-- OPT  tx_cal_busy.tx_cal_busy
		);
	end component tx_reset;

	-- Signals
	signal user_rst, powerdown, analogreset, digitalreset, plllocked, cal_busy: std_logic := '0';
	signal reconfig2xcvr : std_logic_vector(139 downto 0) := (others => '0');
	signal reconfigFxcvr : std_logic_vector(91 downto 0)  := (others => '0');
	
	
begin

	transiver: transiver24 port map(
			tx_pll_refclk(0) 			=> CLK,
			tx_serial_data(0) 		=> tx_signal,
			tx_pma_parallel_data(79 downto 40) 	=> (others => '0'),
			tx_pma_parallel_data(39 downto 30) 	=> "00" & data(31 downto 24),
			tx_pma_parallel_data(29 downto 20) 	=> "00" & data(23 downto 16),
			tx_pma_parallel_data(19 downto 10) 	=> "00" & data(15 downto  8),
			tx_pma_parallel_data( 9 downto  0) 	=> "00" & data( 7 downto  0),
			tx_pma_clkout(0) 			=> clk_out,
			tx_cal_busy(0) 			=> cal_busy,
			pll_powerdown(0) 			=> powerdown,
			tx_analogreset(0) 		=> analogreset,
			tx_digitalreset(0) 		=> digitalreset,
			pll_locked(0) 				=> plllocked,
			reconfig_to_xcvr 			=> reconfig2xcvr,
			reconfig_from_xcvr 		=> reconfigFxcvr);
		
	reset : tx_reset port map(
			clock 						=> CLK,
			reset 						=> RST,
			pll_locked(0) 				=> plllocked,
			pll_select(0) 				=> '0',
			tx_cal_busy(0) 			=> cal_busy,
			pll_powerdown(0) 			=> powerdown,
			tx_analogreset(0) 		=> analogreset,
			tx_digitalreset(0) 		=> digitalreset,
			tx_ready(0) 				=> ready);
		
	reconfig : tx_reconf port map(
			mgmt_clk_clk 				=> CLK,
			mgmt_rst_reset 			=> RST,
			reconfig_mgmt_read 		=> '0',
			reconfig_mgmt_write 		=> '0',
			reconfig_from_xcvr 		=> reconfigFxcvr,
			reconfig_mgmt_address 	=> (others => '0'),
			reconfig_mgmt_writedata => (others => '0'),
			reconfig_busy 				=> busy,
			reconfig_to_xcvr 			=> reconfig2xcvr);

end ar_tx;
	